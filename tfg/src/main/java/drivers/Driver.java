package drivers;

import java.util.List;

import containers.Container;
import containers.ContainerInfo;

public interface Driver {
	boolean connect();
	boolean disconnect();
	List<Container> getContainers();
	ContainerInfo getContainerInfo(Container container);
	boolean stopContainer(Container x);
}
