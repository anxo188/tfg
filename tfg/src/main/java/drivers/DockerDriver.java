package drivers;

import java.util.List;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Info;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

import containers.Container;
import containers.ContainerInfo;
import containers.ContainerInfoDocker;

public class DockerDriver implements Driver {
	DockerClientConfig config;
	DockerClient dockerClient;
	
	/** Example variables
	 * DOCKER_HOST The Docker Host URL, e.g. tcp://localhost:2376 or unix:///var/run/docker.sock
	 * DOCKER_TLS_VERIFY enable/disable TLS verification (switch between http and https protocol)
	 * DOCKER_CERT_PATH Path to the certificates needed for TLS verification
	 * DOCKER_CONFIG Path for additional docker configuration files (like .dockercfg)
	 * api.version The API version, e.g. 1.23.
	 * registry.url Your registry's address.
	 * registry.username Your registry username (required to push containers).
	 * registry.password Your registry password.
	 * registry.email Your registry email.
	 */
	
	public DockerDriver() {
		this.config = DefaultDockerClientConfig.createDefaultConfigBuilder()
				  .withDockerHost("tcp://localhost:2375")
				  .withDockerTlsVerify(false)
				  .build();
		connect();
				
	}

	public DockerDriver(String hostdir,String confDir, String registryUrl,String registryUsername,String registryPassword, String registryEmail) {
		this.config = DefaultDockerClientConfig.createDefaultConfigBuilder()
			    .withDockerHost(hostdir)
			    .withDockerTlsVerify(false)
			    .withDockerConfig(confDir)
			    .withRegistryUrl(registryUrl)
			    .withRegistryUsername(registryUsername)
			    .withRegistryPassword(registryPassword)
			    .withRegistryEmail(registryEmail)
			    .build();
		this.dockerClient = DockerClientBuilder.getInstance(config).build();
	}
	public DockerDriver(String hostdir,String CertPath,String confDir,String registryUrl,String registryUsername, String registryPassword, String registryEmail) {
		this.config = DefaultDockerClientConfig.createDefaultConfigBuilder()
			    .withDockerHost(hostdir)
			    .withDockerTlsVerify(true)
			    .withDockerCertPath(CertPath)
			    .withDockerConfig(confDir)
			    .withRegistryUrl(registryUrl)
			    .withRegistryUsername(registryUsername)
			    .withRegistryPassword(registryPassword)
			    .withRegistryEmail(registryEmail)
			    .build();
	}
	
	@Override
	public boolean connect() {
		DockerClientConfig configuration = this.config;
		try {
			this.dockerClient = DockerClientBuilder.getInstance(configuration).build();
		}catch(Exception e) {
			System.out.println("Error detected while connecting to host: "+e.getLocalizedMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean disconnect(){
		try {
			this.dockerClient.close();
		}catch(Exception e) {
		System.out.println("Error detected while disconnecting from host: "+e.getLocalizedMessage());
		return false;
		}
		return true;
	}

	@Override
	public List<Container> getContainers() {
		return Container.parserList(this.dockerClient.listContainersCmd().exec());
	}

	@Override
	public ContainerInfo getContainerInfo(Container container) {
		
		return new ContainerInfoDocker( this.dockerClient.inspectContainerCmd(container.getId()).exec() );
	}

	@Override
	public boolean stopContainer(Container container) {
		this.dockerClient.stopContainerCmd(container.getId()).exec();
		return true;
	}
	
	public Info getInfo() {
		return this.dockerClient.infoCmd().exec();
	}

}
