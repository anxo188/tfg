package plugins;

public interface PluginManager {
	void loadFromResources(String path);
	void loadNativePlugins();
	Plugin getAvailablePlugins();
	void enablePlugin(Plugin target);
	void disablePlugin(Plugin target);
}
