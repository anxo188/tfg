package host;

import java.util.List;

import com.github.dockerjava.api.model.Info;

import containers.Container;

public interface HostConnection {
	 public Info getInfo();
	 public List<Container> getContainers();
	 public boolean connect();
	 public boolean disconnect();
}
