package host;

import java.util.List;

import com.github.dockerjava.api.model.Info;

import containers.Container;
import drivers.DockerDriver;

public class HostConnectionDocker implements HostConnection{
	DockerDriver driver;
	
	public HostConnectionDocker() {
		driver = new DockerDriver();
	}
	
	public Info getInfo() {
		return driver.getInfo();
	};
	
	public List<Container> getContainers(){
		return driver.getContainers();
	}
	public boolean connect() {
		return driver.connect();
	}
	public boolean disconnect() {
		return driver.disconnect();
	}
}
