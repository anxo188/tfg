package host;

import java.util.List;

import com.google.gson.Gson;

import containers.Container;


public class Host {
	HostInfo info;
	HostConnection connection;
	List<Container> containers;

	
	public Host() {
		
		connection = null;
	}
	
	public Host(HostInfo info) {
		
	}
	
	public Host(String hostname,String hostAlias,int port,String type) {
		
		switch(type) {
			case "docker": 
					
					connection = new HostConnectionDocker();
					info = new HostInfoDocker(hostname,hostAlias,port,null,connection);
					this.containers = connection.getContainers();
					
					break;
		}
	}
	
	public Host createFormJson(String json) {
		Gson gson = new Gson();
		
		
		return null;
	}
	

	public List<Container> getContainers() {
		return containers;
	}

	public void setContainers(List<Container> containers) {
		this.containers = containers;
	}

	public HostInfo getInfo() {
		return info;
	}

	public void setInfo(HostInfo info) {
		this.info = info;
	}

	public HostConnection getConnection() {
		return connection;
	}

	public void setConnection(HostConnection connection) {
		this.connection = connection;
	}
	
	public boolean connect() {
		return this.connection.connect();
	}
	
	public boolean disconnect(){
		return this.connection.disconnect();
	}
	
	public String toJson() {
			
		return this.info.toJson();
		
	}
	
}
