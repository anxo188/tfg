package host;

import com.github.dockerjava.api.model.Info;

public class HostInfoDocker extends HostInfo {
		
	
		private Info generalInfo;
	
		public HostInfoDocker(String hostname,
				String hostAlias,
				int port,
				String type,HostConnection connection) {
			super( hostname,
					hostAlias,
					port,
					"docker", connection);
			this.generalInfo = connection.getInfo();
			
			this.architecture = generalInfo.getArchitecture();
			this.numContainers = generalInfo.getContainers();
			this.numImages = generalInfo.getImages();
			this.availableCpu = generalInfo.getNCPU();
			this.availableMemory = generalInfo.getMemTotal() * (2*2^20); 
		}
		
		public Info getInfo() {
			return this.generalInfo;
		}
		
		public void setInfo(Info info) {
			this.generalInfo=info;
		}
		
	
}
