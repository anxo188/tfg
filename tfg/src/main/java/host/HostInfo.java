package host;

import com.google.gson.Gson;

public abstract class HostInfo {
	//Dirección con la que conectarse al host
	private String hostname;
	//Nombre del host definido por el usuario
	private String hostAlias;
	//Puerto en el que se sirve la api de docker
	private int port;
	//Tipo de servicio ofrecido por el host docker,kubernetes, etc
	private String type;
	String architecture;
	int numContainers;
	int numImages;
	Long availableMemory;
	int availableCpu;
	
	public HostInfo() {
		this.hostname=null;
		this.hostAlias=null;
		this.port=0;
		this.type=null;
	}
	
	public HostInfo( 
				String hostname,
				String hostAlias,
				int port,
				String type,HostConnection connection) {
		this.hostname=hostname;
		this.hostAlias=hostAlias;
		this.port=port;
		this.type=type;
	
	}
	
	
	

	public String getArchitecture() {
		return architecture;
	}

	public void setArchitecture(String architecture) {
		this.architecture = architecture;
	}

	public int getNumContainers() {
		return numContainers;
	}

	public void setNumContainers(int numContainers) {
		this.numContainers = numContainers;
	}

	public int getNumImages() {
		return numImages;
	}

	public void setNumImages(int numImages) {
		this.numImages = numImages;
	}

	public Long getAvailableMemory() {
		return availableMemory;
	}

	public void setAvailableMemory(Long availableMemory) {
		this.availableMemory = availableMemory;
	}

	public int getAvailableCpu() {
		return availableCpu;
	}

	public void setAvailableCpu(int availableCpu) {
		this.availableCpu = availableCpu;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getHostAlias() {
		return hostAlias;
	}

	public void setHostAlias(String hostAlias) {
		this.hostAlias = hostAlias;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String toJson() {
		Gson gson = new Gson();
		return gson.toJson(this);
		
	}
	
	
}
