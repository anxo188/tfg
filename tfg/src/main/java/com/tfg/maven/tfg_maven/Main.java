/**
 * 
 */
package com.tfg.maven.tfg_maven;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import com.formdev.flatlaf.FlatDarkLaf;

import host.Host;
import interfaces.MainFrame;

/**
 * 
 * 
 */
public class Main {

	/**
	 * @param args
	 * 
	 */
	
	public static void main(String[] args) {
		
		FlatDarkLaf.install();
		Host local = new Host("localhost","Localhost",2375,"docker");
		System.out.println(local.toJson());
		
		//List<Host> hosts = new LinkedList();
		//hosts.add(local);
		List<Host> hostList = new LinkedList();
		
		//Recommended Sun use of special thread to run the interface
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainFrame frame = new MainFrame(hostList);			
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(1280,720);
				frame.setVisible(true);	
			}
		});

	}


}
