package interfaces;

import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JLabel;

import host.Host;

public class MainFrame extends JFrame{

	
	private JLabel listLabel;
	private JButton addButton;
	
	public MainFrame(List<Host> hostList) {
		super("VisualDockerManager");		
		
		//Layout Manager
		setLayout(new BorderLayout());
		
		//Swing components
		listLabel = new JLabel("Connected Host List");
		addButton = new JButton("Add new Host");
		
		//Adding swing components to content pane
		Container container = getContentPane();
		
		container.add(listLabel,BorderLayout.NORTH);
		container.add(addButton,BorderLayout.SOUTH);
		
		
		Iterator<Host> iterator = hostList.iterator();
		while(iterator.hasNext()) {
			Host host = iterator.next();
			
			JLabel hostname = new JLabel(host.getInfo().getHostAlias());
			container.add(hostname);
		}
		
		addButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				container.removeAll();

				
				JButton backButton = new JButton("Back");
				AddHostView view = new AddHostView(hostList);
				
				container.add(view,BorderLayout.CENTER);
				container.add(backButton,BorderLayout.SOUTH);

				
				backButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						container.remove(view);
						container.remove(backButton);
						container.add(listLabel,BorderLayout.NORTH);
						container.add(addButton,BorderLayout.SOUTH);
						Iterator<Host> iterator = hostList.iterator();
						while(iterator.hasNext()) {
							Host host = iterator.next();
							
							JLabel hostname = new JLabel(host.getInfo().getHostAlias());
							container.add(hostname);
						}
						container.repaint();
						container.revalidate();
						
					}
				});
				
				container.repaint();
				container.revalidate();
			}
		});
			
	}
	
	public void addHost() {
		
	}
	
	
}
