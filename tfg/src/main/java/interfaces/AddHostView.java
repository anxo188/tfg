package interfaces;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import host.Host;

public class AddHostView extends JPanel {
	private JButton backButton;
	public AddHostView(List<Host> hostList ) {
		this.backButton = backButton;
		Dimension size = getPreferredSize();
		size.width = 250;
		setPreferredSize(size);
		setBorder(BorderFactory.createTitledBorder("Add new host"));

		setLayout(new GridBagLayout());

		GridBagConstraints gc = new GridBagConstraints();

		// First column
		JLabel hostNameLabel = new JLabel("Host Alias: ");
		JLabel hostDirectionLabel = new JLabel("Host Direction: ");
		JLabel hostPortLabel = new JLabel("Host Port: ");
		JLabel hostTypeLabel = new JLabel("Host Type: ");
		JLabel hostCertUse = new JLabel("Use Certificate?");
		JLabel hostCertPath = new JLabel("Certifitace Path: ");

		gc.weightx = 0.5;
		gc.weighty = 0.5;
		gc.anchor = GridBagConstraints.LINE_END;

		gc.gridy = 0;
		gc.gridx = 0;
		add(hostNameLabel, gc);
		gc.gridy = 1;
		add(hostDirectionLabel, gc);
		gc.gridy = 2;
		add(hostPortLabel, gc);
		gc.gridy = 3;
		add(hostTypeLabel, gc);

				
		// Second column
		JTextField hostName = new JTextField(15);
		hostName.setText("Local Docker");
		JTextField hostDirection = new JTextField(50);
		hostDirection.setText("localhost");
		NumberFormat format = NumberFormat.getInstance();
		format.setGroupingUsed(false);
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(0);
		formatter.setMaximum(65535);
		formatter.setAllowsInvalid(false);
		JFormattedTextField hostPort = new JFormattedTextField(formatter);
		hostPort.setValue(2375);
		
		JCheckBox useCert = new JCheckBox();
		JTextField certPath = new JTextField(50);
		
		
		
		
		
		
		
		
		
		
		
		
		JComboBox hostTypeSelector = new JComboBox();
		hostTypeSelector.addItem("Docker");

		gc.anchor = GridBagConstraints.LINE_START;
		gc.gridx = 1;
		gc.gridy = 0;
		add(hostName, gc);
		gc.gridy = 1;
		add(hostDirection, gc);
		gc.gridy = 2;
		add(hostPort, gc);
		gc.gridy = 3;
		add(hostTypeSelector, gc);
		
		gc.gridx = 3;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.LINE_END;
		JButton addButton = new JButton("Confirm and add");
		add(addButton,gc);
		
		
		
		
		switch(hostTypeSelector.getSelectedItem().toString()) {
		case "Docker":
		default:
			gc.anchor = GridBagConstraints.LINE_END;
			gc.gridx = 0;
			gc.gridy = 8;
			add(hostCertUse, gc);
			gc.gridy = 10;
			add(hostCertPath, gc);
			gc.anchor = GridBagConstraints.LINE_START;
			gc.gridx = 1;
			gc.gridy = 8;
			add(useCert,gc);
			gc.gridy = 10;
			add(certPath,gc);
		}
		
		addButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {

				String type;
				
				switch(hostTypeSelector.getSelectedItem().toString()) {
				case "Docker":
				default:
					 type = "docker";
				}
				JLabel message;
				try {
					hostList.add( new Host( hostDirection.getText(), hostName.getText(),Integer.parseInt(hostPort.getText()),type));
					message = new JLabel("Added succesfully");
					System.out.println("Exito");
				}catch(Exception e) {
					System.out.println(e.getMessage());
					 message = new JLabel("Unexpected error");
				}
				JDialog dialog = new JDialog();
				dialog.add(message);
				dialog.setSize(100,100);
				dialog.setVisible(true);
				
				
				
			}
		});
		
		hostTypeSelector.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switch(hostTypeSelector.getSelectedItem().toString()) {
				case "Docker":
					gc.anchor = GridBagConstraints.LINE_END;
					gc.gridx = 0;
					gc.gridy = 8;
					add(hostCertUse, gc);
					gc.gridy = 10;
					add(hostCertPath, gc);
					break;
				default:
					remove(hostCertUse);
					remove(hostCertPath);
					break;
				}
				repaint();
				revalidate();
			}
		});
		
		
		
			
		
		

	}
}
