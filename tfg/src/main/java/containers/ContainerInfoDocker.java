package containers;

import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.model.ContainerConfig;
import com.github.dockerjava.api.model.ContainerPort;
import com.github.dockerjava.api.model.ExposedPort;

public class ContainerInfoDocker implements ContainerInfo{
	private int[] exposedPorts;
	private String[] asociatedProtocol;
	private String imageName;
	private String name;
	private String status;
	private String id;

	public ContainerInfoDocker(InspectContainerResponse inspectContainerResponse) {
		ContainerConfig containerConfig = inspectContainerResponse.getConfig();
		name = inspectContainerResponse.getName();
		status = inspectContainerResponse.getState().toString();
		imageName = containerConfig.getImage();
		id=inspectContainerResponse.getId();
		ExposedPort[] ports = containerConfig.getExposedPorts();
		exposedPorts = new int[ports.length];
		asociatedProtocol = new String[ports.length];

		for(int i =0; i< ports.length;i++) {
			exposedPorts[i] = ports[i].getPort();
			asociatedProtocol[i] = ports[i].getProtocol().toString();
		}
		
	}
	

	public ContainerInfoDocker(ContainerPort[] ports, String name,String id, String imageName, String status) {
		this.name =name;
		this.id = id;
		this.status  =status;
		this.imageName = imageName;
		exposedPorts = new int[ports.length];
		asociatedProtocol = new String[ports.length];

		for(int i =0; i< ports.length;i++) {
			exposedPorts[i] = ports[i].getPublicPort();
			asociatedProtocol[i] = ports[i].getType();
		}
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public int[] getExposedPorts() {
		return exposedPorts;
	}

	public void setExposedPorts(int[] exposedPorts) {
		this.exposedPorts = exposedPorts;
	}

	public String[] getAsociatedProtocol() {
		return asociatedProtocol;
	}

	public void setAsociatedProtocol(String[] asociatedProtocol) {
		this.asociatedProtocol = asociatedProtocol;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
