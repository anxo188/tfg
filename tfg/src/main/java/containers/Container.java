package containers;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;



public class Container {
	private ContainerInfo info;
	
	public ContainerInfo getInfo() {
		return info;
	}

	public void setInfo(ContainerInfo info) {
		this.info = info;
	}
	
	public Container(ContainerInfo info) {
		this.info = info;
	}
	
	public static List<Container> parserList(List<com.github.dockerjava.api.model.Container> containerList) {
		Iterator<com.github.dockerjava.api.model.Container> iterator =  containerList.iterator();
		List<Container> list = new LinkedList<Container>(); 
		while (iterator.hasNext()) {
			com.github.dockerjava.api.model.Container actual = iterator.next();
			
			ContainerInfoDocker aux = new ContainerInfoDocker(actual.getPorts(),actual.getNames().toString(),actual.getId(),actual.getImage(), actual.getState());
			list.add(new Container(aux));
		}
		
		return list;
		
	}
	
	public String getId() {
		return info.getId();
	}
}
