package containers;

public interface ContainerInfo {

	public String getId();
	
	public void setId(String id);

	public int[] getExposedPorts();

	public void setExposedPorts(int[] exposedPorts);

	public String[] getAsociatedProtocol();

	public void setAsociatedProtocol(String[] asociatedProtocol);

	public String getImageName();

	public void setImageName(String imageName);

	public String getName();

	public void setName(String name);

	public String getStatus();

	public void setStatus(String status);
}
