/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.plugins;

import org.pf4j.ExtensionPoint;

import es.uvigo.tfg.mantoc.core.Container;
import es.uvigo.tfg.mantoc.gui.PluginViewCloseListener;

/**
 *
 * @author anxo
 */
public interface ContainerPlugin extends ExtensionPoint {
    public PluginInfo getInfo();
    public boolean isApplicableTo(Container container);
    public PluginView applyTo(Container container,PluginViewCloseListener listener);
}
