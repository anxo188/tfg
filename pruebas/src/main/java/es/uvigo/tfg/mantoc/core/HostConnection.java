/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.core;

/**
 *
 * @author anxo
 */
public interface HostConnection {
	public Host getHost();
	public Driver getDriver();

}
