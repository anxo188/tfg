package es.uvigo.tfg.mantoc.docker;

import java.util.ArrayList;
import java.util.List;

import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.model.ContainerConfig;
import com.github.dockerjava.api.model.InternetProtocol;

import es.uvigo.tfg.mantoc.core.ContainerInfo;
import es.uvigo.tfg.mantoc.core.ExposedPort;

public class DockerContainerInfo extends ContainerInfo{
	
	private String imageName;
	private InspectContainerResponse inspector;
	
	public DockerContainerInfo(InspectContainerResponse inspectContainerResponse) {
		super();	
		this.setInspector(inspectContainerResponse);
		ContainerConfig containerConfig = inspectContainerResponse.getConfig();
		this.name = inspectContainerResponse.getName();
		this.status = inspectContainerResponse.getState().getStatus();
		this.imageName = containerConfig.getImage();
		this.id=inspectContainerResponse.getId();
		com.github.dockerjava.api.model.ExposedPort[] ports = containerConfig.getExposedPorts();	
		List<ExposedPort> portsClean = new ArrayList<ExposedPort>();
		if(ports != null) {
			for(int i =0; i< ports.length;i++) {
				int dockerPort = ports[i].getPort();
				InternetProtocol protocol = ports[i].getProtocol();
				ExposedPort port = new ExposedPort(Integer.toString(dockerPort),null,protocol.toString());
				portsClean.add(port);
			}			
		}
		this.exposedPorts = portsClean;

		
	}


	public String getImageName() {
		return imageName;
	}


	public void setImageName(String imageName) {
		this.imageName = imageName;
	}


	public InspectContainerResponse getInspector() {
		return inspector;
	}


	public void setInspector(InspectContainerResponse inspector) {
		this.inspector = inspector;
	}

	
	

}
