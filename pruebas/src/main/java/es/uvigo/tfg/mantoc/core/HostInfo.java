package es.uvigo.tfg.mantoc.core;

public class HostInfo {
	protected String architecture;
	protected String id;
	protected String systemTime;
	protected int nCpu;
	protected boolean memLimit;
	protected long memTotal;
	protected String oS;
	protected String serverVersion;
	protected int nContainers;
	protected int nContainersPaused;
	protected int nContainersRunning;
	protected int nContainersStopped;
	protected int nImages;

	public String getoS() {
		return oS;
	}

	public void setoS(String oS) {
		this.oS = oS;
	}

	public String getServerVersion() {
		return serverVersion;
	}

	public void setServerVersion(String serverVersion) {
		this.serverVersion = serverVersion;
	}

	public String getArchitecture() {
		return architecture;
	}

	public void setArchitecture(String architecture) {
		this.architecture = architecture;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystemTime() {
		return systemTime;
	}

	public void setSystemTime(String systemTime) {
		this.systemTime = systemTime;
	}

	public int getnCpu() {
		return nCpu;
	}

	public void setnCpu(int nCpu) {
		this.nCpu = nCpu;
	}

	public boolean getMemLimit() {
		return memLimit;
	}

	public void setMemLimit(boolean memLimit) {
		this.memLimit = memLimit;
	}

	public long getMemTotal() {
		return memTotal;
	}

	public void setMemTotal(long memTotal) {
		this.memTotal = memTotal;
	}

	public int getnContainers() {
		return nContainers;
	}

	public void setnContainers(int nContainers) {
		this.nContainers = nContainers;
	}

	public int getnContainersPaused() {
		return nContainersPaused;
	}

	public void setnContainersPaused(int nContainersPaused) {
		this.nContainersPaused = nContainersPaused;
	}

	public int getnContainersRunning() {
		return nContainersRunning;
	}

	public void setnContainersRunning(int nContainersRunning) {
		this.nContainersRunning = nContainersRunning;
	}

	public int getnContainersStopped() {
		return nContainersStopped;
	}

	public void setnContainersStopped(int nContainersStopped) {
		this.nContainersStopped = nContainersStopped;
	}

	public int getnImages() {
		return nImages;
	}

	public void setnImages(int nImages) {
		this.nImages = nImages;
	}

}
