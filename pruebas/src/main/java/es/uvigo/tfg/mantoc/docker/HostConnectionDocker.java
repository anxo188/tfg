package es.uvigo.tfg.mantoc.docker;

import com.github.dockerjava.api.DockerClient;

import es.uvigo.tfg.mantoc.core.Driver;
import es.uvigo.tfg.mantoc.core.Host;
import es.uvigo.tfg.mantoc.core.HostConnection;

public class HostConnectionDocker implements HostConnection{

	private DockerClient client;
	private Host host;
	private Driver driver;

	
	public HostConnectionDocker(DockerClient client,Host host, Driver driver) {
		this.client=client;
		this.host=host;
		this.driver=driver;
	
	}
	
	public DockerClient getDockerClient() {
		return this.client;
	}

	@Override
	public Host getHost() {
		return this.host;
	}

	@Override
	public Driver getDriver() {
		return this.driver;
	}
	
}
