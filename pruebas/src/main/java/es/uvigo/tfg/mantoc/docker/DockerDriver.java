package es.uvigo.tfg.mantoc.docker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.pf4j.Extension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

import es.uvigo.tfg.mantoc.core.Container;
import es.uvigo.tfg.mantoc.core.ContainerInfo;
import es.uvigo.tfg.mantoc.core.Driver;
import es.uvigo.tfg.mantoc.core.DriverInfo;
import es.uvigo.tfg.mantoc.core.Host;
import es.uvigo.tfg.mantoc.core.HostConnection;
import es.uvigo.tfg.mantoc.core.HostInfo;
import es.uvigo.tfg.mantoc.plugins.MainPluginManager;

/**
*
* @author anxo
*/
@Extension
public class DockerDriver implements Driver {
	DriverInfo driverInfo = new DriverInfo("Docker", "Basic driver dor docker implemented using docker-java api","es.uvigo.ccia.pruebas.plugins.core.drivers.DockerDriver");
	final Logger logger = LoggerFactory.getLogger(MainPluginManager.class);
	/** Example variables
	 * DOCKER_HOST The Docker Host URL, e.g. tcp://localhost:2376 or unix:///var/run/docker.sock
	 * DOCKER_TLS_VERIFY enable/disable TLS verification (switch between http and https protocol)
	 * DOCKER_CERT_PATH Path to the certificates needed for TLS verification
	 * DOCKER_CONFIG Path for additional docker configuration files (like .dockercfg)
	 * api.version The API version, e.g. 1.23.
	 * registry.url Your registry's address.
	 * registry.username Your registry username (required to push containers).
	 * registry.password Your registry password.
	 * registry.email Your registry email.
	 */
	
	public DockerDriver() {				
	}

	
	public DriverInfo getDriverInfo() {
		return this.driverInfo;
	}
	

	public HostConnection connect(Host host) {
		logger.info("Connecting to host: "+host.getName());
		DockerClientConfig config;
		DockerClient client;
		config = DefaultDockerClientConfig.createDefaultConfigBuilder()
				  .withDockerHost("tcp://"+host.getDirection()+":"+host.getPort())
				  .withDockerTlsVerify(false)
				  .build();
		DockerClientConfig configuration = config;
		try {
			
			client = DockerClientBuilder.getInstance(configuration).build();
			HostConnectionDocker connection = new HostConnectionDocker(client,host,this);
			logger.info("Connected succesfully");
			return connection;
		}catch(Exception e) {
			logger.error("Error detected while connecting to host: "+e.getLocalizedMessage());
			return null;
		}
		
	}


	public boolean disconnect(Host host){
		try {
			HostConnectionDocker connection = (HostConnectionDocker) host.getHostConnection();
			connection.getDockerClient().close();
			host.setHostConnection(null);
		}catch(Exception e) {
			System.out.println("Error detected while disconnecting from host: "+e.getLocalizedMessage());
			return false;
		}
			return true;
	}

	@Override
	public List<Container> getContainers(Host host) {
		logger.info("Obtaining containers of host: "+host.getName());
		HostConnectionDocker connection = (HostConnectionDocker) host.getHostConnection();
		DockerClient client = connection.getDockerClient();
		List<com.github.dockerjava.api.model.Container> containers = client.listContainersCmd().withStatusFilter(Arrays.asList("created","restarting","running","paused","exited")).exec();
		
		List<Container> containersClean = new ArrayList<Container>();
		Iterator<com.github.dockerjava.api.model.Container> it = containers.iterator();
		while(it.hasNext()) {
			com.github.dockerjava.api.model.Container current = it.next();
			containersClean.add(new DockerContainer(current,host));		
		}
		logger.info(containersClean.size()+" Containers obtained");
		return containersClean;
	}

	@Override
	public HostInfo getHostInfo(Host host) {
		HostConnectionDocker connectionDocker = (HostConnectionDocker) host.getHostConnection();
		DockerClient client = connectionDocker.getDockerClient();
		
		return new HostInfoDocker(client.infoCmd().exec());
	}

	@Override
	public ContainerInfo getContainerInfo(Container container) {
		HostConnectionDocker connectionDocker = (HostConnectionDocker) container.getOwnerHost().getHostConnection();
		DockerClient client = connectionDocker.getDockerClient();
		InspectContainerResponse inspectContainerResponse = client.inspectContainerCmd(container.getId()).exec();
		return new DockerContainerInfo(inspectContainerResponse);
	}


	@Override
	public boolean stopContainer(Container container) {
		HostConnectionDocker connectionDocker = (HostConnectionDocker) container.getOwnerHost().getHostConnection();
		DockerClient client = connectionDocker.getDockerClient();
		client.stopContainerCmd(container.getId()).exec();
		if(container.getContainerInfo()!= null)
			container.getContainerInfo().setStatus("Exited");
		return true;
	}


	@Override
	public boolean haltContainer(Container container) {
		HostConnectionDocker connectionDocker = (HostConnectionDocker) container.getOwnerHost().getHostConnection();
		DockerClient client = connectionDocker.getDockerClient();
		client.stopContainerCmd(container.getId()).exec();
		client.pauseContainerCmd(container.getId()).exec();
		if(container.getContainerInfo()!= null)
			container.getContainerInfo().setStatus("Halted");
		return true;
	}


	@Override
	public boolean resumeContainer(Container container) {
		HostConnectionDocker connectionDocker = (HostConnectionDocker) container.getOwnerHost().getHostConnection();
		DockerClient client = connectionDocker.getDockerClient();
		client.restartContainerCmd(container.getId()).exec();
		if(container.getContainerInfo()!= null)
			container.getContainerInfo().setStatus("Running");
		return true;
	}



}
