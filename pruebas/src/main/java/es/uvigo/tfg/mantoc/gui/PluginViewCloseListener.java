package es.uvigo.tfg.mantoc.gui;

import es.uvigo.tfg.mantoc.core.Host;
import es.uvigo.tfg.mantoc.plugins.PluginView;

public interface PluginViewCloseListener {
	void notifyClose(Host hostSelected,PluginView pluginView);
}
