/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.gui;

import es.uvigo.tfg.mantoc.core.Container;
import es.uvigo.tfg.mantoc.core.Driver;
import es.uvigo.tfg.mantoc.core.Host;
import es.uvigo.tfg.mantoc.plugins.ContainerPlugin;
import es.uvigo.tfg.mantoc.plugins.HostPlugin;
import es.uvigo.tfg.mantoc.plugins.MainPluginManager;
import es.uvigo.tfg.mantoc.plugins.PluginView;
import net.miginfocom.swing.MigLayout;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author anxo
 */
public class MainController implements PluginViewCloseListener {

 	final Logger logger = LoggerFactory.getLogger(MainPluginManager.class);
 	
	private MainWindow view;
	private List<Host> hosts;

	private MainPluginManager pluginManager;
	private Map<Host,List<PluginView>> asociatedPanels;
	private List<ContainerPlugin> availableContainerPlugins;
	private List<HostPlugin> availableHostPlugins;
	private List<Driver> availableDrivers;
	private String saveFilePath="./hosts.json";

	public MainController() {
		this.pluginManager = new MainPluginManager();
		this.availableContainerPlugins = new ArrayList<>();
		this.availableHostPlugins = new ArrayList<>();
		this.availableDrivers = new ArrayList<>();
		this.hosts = new ArrayList<>();
		this.asociatedPanels= new HashMap<Host,List<PluginView>>();
	}

	public void loadCorePlugins() {
		this.pluginManager.loadCorePlugins();

		this.availableHostPlugins = this.pluginManager.getAvailableHostPlugins();
		this.availableContainerPlugins = this.pluginManager.getAvailableContainerPlugins();
		this.availableDrivers = this.pluginManager.getAvailableDriversList();
	}

	public void loadHosts() {
		
		
		try (Reader reader = new FileReader(this.saveFilePath)) {
			Gson gson = new Gson();
			Type tipo = new TypeToken<List<Host>>() {
			}.getType();
			List<Host> nuevoHosts = gson.fromJson(reader, tipo);
			if(nuevoHosts !=null && !nuevoHosts.isEmpty()) {
				
				this.hosts=nuevoHosts;
				Iterator<Host> it = nuevoHosts.iterator();
				while(it.hasNext()) {
					Host current = it.next();
					current.setNullContainer();
				}
				
				if(view != null)
					view.showMessage(nuevoHosts.size()+" Hosts loaded");
				logger.info(nuevoHosts.size()+" Hosts loaded");
			}else {
				if(view != null)
					view.showMessage("Empty file, 0 Host loaded");
				logger.info("Empty file, 0 Host loaded");
			}

			 
		} catch (IOException e) {
			 logger.info("Error detecting file of saved hosts: "+this.saveFilePath);
			 if(view != null)
				 view.showMessage("Couldn't load hosts, save file may be lost");
		}catch( Exception e) {
			logger.error("Error while loading hosts: "+e.getLocalizedMessage());
			 if(view != null)
				 view.showMessage("Couldn't load hosts, unexpected error");
			
		}

	}
	
public void loadHosts(File file) {
		
		
		try (Reader reader = new FileReader(file)) {
			Gson gson = new Gson();
			Type tipo = new TypeToken<List<Host>>() {
			}.getType();
			List<Host> nuevoHosts = gson.fromJson(reader, tipo);
			/*if(nuevoHosts != null) {
				Iterator<Host> it = nuevoHosts.iterator();
				while(it.hasNext()) {
					Host current = it.next();
					current.setNullContainer();
				}
				this.hosts.addAll(nuevoHosts);
				
			}*/
			
			if(nuevoHosts !=null && !nuevoHosts.isEmpty()) {
				
				this.hosts=nuevoHosts;
				Iterator<Host> it = nuevoHosts.iterator();
				while(it.hasNext()) {
					Host current = it.next();
					current.setNullContainer();
				}
				
				if(view != null)
					view.showMessage(nuevoHosts.size()+" Hosts loaded");
				logger.info(nuevoHosts.size()+" Hosts loaded");
			}else {
				if(view != null)
					view.showMessage("Empty file, 0 Host loaded");
				logger.info("Empty file, 0 Host loaded");
			}

			 
		} catch (IOException e) {
			 logger.info("Error detecting file of saved hosts: "+this.saveFilePath);
			 if(view != null)
				 view.showMessage("Couldn't load hosts, save file may be lost");
		}

	}

	public void saveHosts() {
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().excludeFieldsWithoutExposeAnnotation()
				.create();
		String jsonChain = gson.toJson(hosts);
		try(	FileWriter fwObject = new FileWriter(this.saveFilePath, false);
		        PrintWriter pwObject = new PrintWriter(fwObject, false)){
			pwObject.flush();
			fwObject.write(jsonChain);
			pwObject.close();
			fwObject.close();
			view.showMessage("Hosts saved");
		} catch (IOException e) {
			logger.info("Error saving hosts in file of saved hosts: "+this.saveFilePath);
			view.showMessage("Couldn't save hosts");
		}
	}
	

	public void saveHosts(File file) {
		
		File renamedFile = new File(file.getAbsolutePath()+".json");
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().excludeFieldsWithoutExposeAnnotation()
				.create();
		String jsonChain = gson.toJson(hosts);
		try(	FileWriter fwObject = new FileWriter(renamedFile, false);
		        PrintWriter pwObject = new PrintWriter(fwObject, false)){
			pwObject.flush();
			fwObject.write(jsonChain);
			pwObject.close();
			fwObject.close();
			view.showMessage("Hosts saved");
		} catch (IOException e) {
			logger.info("Error saving hosts in file of saved hosts: "+this.saveFilePath);
			view.showMessage("Couldn't save hosts");
		}
	}
	
	public void updateHostTree() {
		List<HostTreeModel.HostNode> hostNodes = new ArrayList<>();
		for (Host host : this.hosts) {
			List<HostPlugin> plugins = collectHostPluginsFor(host);
			List<HostTreeModel.ContainerNode> containerNodes = new ArrayList<>();
			for (Container container : host.getContainers()) {
				List<ContainerPlugin> containerPlugins = collectContainerPluginsFor(container);
				containerNodes.add(new HostTreeModel.ContainerNode(container, containerPlugins));
			}
			hostNodes.add(new HostTreeModel.HostNode(host, containerNodes, plugins));
		}
		this.view.updateHostTreeModel(hostNodes);
	}

	private List<HostPlugin> collectHostPluginsFor(Host host) {
		List<HostPlugin> plugins = new ArrayList<>();
		for (HostPlugin plugin : this.availableHostPlugins) {
			if (plugin.isApplicableTo(host)) {
				plugins.add(plugin);
			}
		}
		return plugins;
	}

	private List<ContainerPlugin> collectContainerPluginsFor(Container container) {
		List<ContainerPlugin> plugins = new ArrayList<>();
		for (ContainerPlugin plugin : this.availableContainerPlugins) {
			if (plugin.isApplicableTo(container)) {
				plugins.add(plugin);
			}
		}
		return plugins;
	}

	public void addHost(Host host) {
		if(hosts.contains(host)) {
			view.showMessage("Host name must be unique");
		}else {
			this.hosts.add(host);
			view.showMessage("Host added succesfully");
			this.updateHostTree();
			
		}
	}

	public void deleteHost(Host host) {
		this.hosts.remove(host);
		List<PluginView> asociatedPanels = this.asociatedPanels.get(host);
		if(asociatedPanels != null && !asociatedPanels.isEmpty()) {
			Iterator<PluginView> it = asociatedPanels.iterator();
			while(it.hasNext()) {
				view.removePluginViewFromTab(it.next());
			}
		}
		view.showMessage("Host deleted succesfully");
		this.updateHostTree();	
	}
	public void deleteAllHosts() {
		Iterator<Host> it = this.hosts.iterator();
		while(it.hasNext()) {
			List<PluginView> asociatedPanels = this.asociatedPanels.get(it.next());
			if(asociatedPanels != null && !asociatedPanels.isEmpty()) {
				Iterator<PluginView> iterator2 = asociatedPanels.iterator();
				while(iterator2.hasNext()) {
					view.removePluginViewFromTab(iterator2.next());
				}
			}
		}
		this.hosts = new ArrayList<>();
		view.showMessage("Hosts deleted succesfully");
		this.updateHostTree();
	}

	public List<Host> getHosts() {
		return hosts;
	}

	public List<Driver> getAvailableDrivers() {
		return availableDrivers;
	}

	public Map<String, Driver> getAvailableDriverMap() {
		return this.pluginManager.getAvailableDriversMap();
	}

	public void setView(MainWindow view) {
		this.view = view;
	}

	public void setHosts(List<Host> hosts) {
		this.hosts = hosts;
		this.updateHostTree();
	}
	public Map<Host,List<PluginView>> getAsociatedPanels() {
		return this.asociatedPanels;
	}
	
	public void addAsociatedPanel(Host host, PluginView asociatedPanel) {
		List<PluginView> list = this.asociatedPanels.get(host);
		if(list == null)
			list = new LinkedList<PluginView>();

		list.add(asociatedPanel);
		
		this.asociatedPanels.put(host,list);
	
	}
	public void removeAsociatedPanel(Host host, PluginView asociatedPanel) {		
		List<PluginView> list = this.asociatedPanels.get(host);
		if(list != null) {
			list.remove(asociatedPanel);			
			this.asociatedPanels.put(host,list);		
		}
			
	}
	public List<HostPlugin> getAvailableHostPlugins() {
		return pluginManager.getAvailableHostPlugins();
	}
	public List<ContainerPlugin> getAvailableContainerPlugins() {
		return pluginManager.getAvailableContainerPlugins();
	}

	public void loadPluginFromResources(String path) {
		this.pluginManager.loadPluingsFromResource(path);
	}

	@Override
	public void notifyClose(Host hostSelected,PluginView pluginView) {
		removeAsociatedPanel(hostSelected,pluginView);
		
	}

}
