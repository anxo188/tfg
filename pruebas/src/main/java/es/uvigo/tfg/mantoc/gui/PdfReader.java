package es.uvigo.tfg.mantoc.gui;

import javax.swing.JPanel;

import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;

public class PdfReader {
	public static JPanel openHelpPDF(){
		String helpFile = "./manual.pdf";
		
		SwingController controller = new SwingController();
		SwingViewBuilder factory = new SwingViewBuilder(controller);
		JPanel viewer = factory.buildViewerPanel();
		controller.getDocumentViewController().setAnnotationCallback(
				new org.icepdf.ri.common.MyAnnotationCallback(controller.getDocumentViewController()));
		controller.openDocument(helpFile);
		
		return viewer;		
	}
}
