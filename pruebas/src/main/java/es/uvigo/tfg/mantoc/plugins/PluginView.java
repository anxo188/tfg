/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.plugins;

import javax.swing.JPanel;

import es.uvigo.tfg.mantoc.core.Host;
import es.uvigo.tfg.mantoc.gui.MainController;
import es.uvigo.tfg.mantoc.gui.PluginViewCloseListener;

@SuppressWarnings("serial")
public class PluginView extends JPanel{
    private String name;
    private String title;
    private Host owner;
    private PluginViewCloseListener listener;


	public PluginView(String title) {
        super();
        this.name = title;
        this.title = title;
    }
	
	public void setPluginCloseListener(PluginViewCloseListener listener) {
		this.listener = listener;
	}
	public Host getOwner() {
		return owner;
	}
	
	public void setOwner(Host owner) {
		this.owner = owner;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    
    public void closeView() {
    	if(listener!=null)
    		listener.notifyClose(owner,this);
    	
    }
}
