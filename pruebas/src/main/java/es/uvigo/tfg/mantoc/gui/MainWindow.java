/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;

import es.uvigo.tfg.mantoc.core.Driver;
import es.uvigo.tfg.mantoc.core.DriverInfo;
import es.uvigo.tfg.mantoc.core.Host;
import es.uvigo.tfg.mantoc.plugins.ContainerPlugin;
import es.uvigo.tfg.mantoc.plugins.HostPlugin;
import es.uvigo.tfg.mantoc.plugins.PluginInfo;
import es.uvigo.tfg.mantoc.plugins.PluginView;
import net.miginfocom.swing.MigLayout;


/**
 *
 * @author anxo
 */
@SuppressWarnings("serial")
public class MainWindow extends JFrame {

    private MainController controller;

    @SuppressWarnings("unused")
	private JMenuBar menuBar;
    @SuppressWarnings("unused")
	private JPanel mainPanel;
    private JTree hostsTree;
    private HostTreeModel hostsTreeModel;
    private JTabbedPane tabbedPane;
 
    public MainWindow(MainController controller) {
        super();
        this.controller = controller;
        this.controller.setView(this);
        initComponents();
        this.controller.updateHostTree();
    }

    private void initComponents() {
        this.setJMenuBar(createMenuBar());
        this.setContentPane(createMainPanel());

        this.setTitle("Mantoc");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setMinimumSize(new Dimension(800, 600));
        
        //this.setExtendedState(MAXIMIZED_BOTH);  // Pantalla completa

        this.pack();
    }

    private Container createMainPanel() {
 
        // Con JSplitPane
        JPanel mainPanel = new JPanel(new BorderLayout());
        JScrollPane treePanel = new JScrollPane(createHostsTree());
        JScrollPane tabsPanel = new JScrollPane(createTabbedPane());
        treePanel.setMinimumSize(new Dimension(250,100));
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, treePanel, tabsPanel);
        splitPane.setResizeWeight(0);
        splitPane.setOneTouchExpandable(true);
        splitPane.setContinuousLayout(true);

        mainPanel.add(splitPane, BorderLayout.CENTER);
        this.mainPanel = mainPanel;
        return mainPanel;
    }

    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        MainWindow parent = this;

        JMenu file = new JMenu("File");
        file.setMnemonic('F');
        JMenuItem importHosts = new JMenuItem("Import host list");
        JMenuItem exportHosts = new JMenuItem("Export host list");
        JMenuItem openHosts = new JMenuItem("Reload host list");
		JMenuItem saveHosts = new JMenuItem("Save host list");
		JMenuItem exit = new JMenuItem("Exit");
        
		importHosts.addActionListener(new ActionListener() {
			@Override
        	public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("JSON","json");
				fileChooser.setFileFilter(filter);
				int selection = fileChooser.showOpenDialog(parent);
				if(selection == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					controller.loadHosts(file);
					controller.updateHostTree();
				}else if(selection == JFileChooser.ERROR_OPTION) {
					showMessage("Error while loading hosts");
				}
        	}
		});
		exportHosts.addActionListener(new ActionListener() {
			@Override
        	public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("JSON","json");
				fileChooser.setFileFilter(filter);
				int selection = fileChooser.showOpenDialog(parent);
				if(selection == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					controller.saveHosts(file);
				}else if(selection == JFileChooser.ERROR_OPTION) {
					showMessage("Error while saving hosts");
				}
        	}
		});
		
		
		openHosts.addActionListener(new ActionListener() {
			@Override
        	public void actionPerformed(ActionEvent e) {
        		controller.loadHosts();
        	}
		});
		saveHosts.addActionListener(new ActionListener() {
			@Override
        	public void actionPerformed(ActionEvent e) {
        		controller.saveHosts();
        	}
		});
		
		exit.addActionListener(new ActionListener() {
			@Override
        	public void actionPerformed(ActionEvent e) {
        		controller.saveHosts();
        		System.exit(0);
        	}
		});
		file.add(importHosts);
		file.add(exportHosts);
        file.add(openHosts);
        file.add(saveHosts);
        file.addSeparator();
        file.add(exit);

        JMenu hosts = new JMenu("Hosts");
        hosts.setMnemonic('H');
        JMenuItem addHost = new JMenuItem("Add new host");
        hosts.add(addHost);
        addHost.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		
        		JDialog dialog = new HostManagementDialog(parent,controller,"Add new host");
        		dialog.setVisible(true);
        	}
        });
        JMenuItem delAllHosts = new JMenuItem("Delete all hosts");
        hosts.add(delAllHosts);
        delAllHosts.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		controller.deleteAllHosts();
        	}
        });
        
        JMenu plugins = new JMenu("Plugins");
        plugins.setMnemonic('P');
        JMenuItem showLoadedPlugins = new JMenuItem("Show loaded plugins");
        plugins.add(showLoadedPlugins);
        showLoadedPlugins.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		showLoadedPlugins();
        	}
        });
        
        JMenuItem loadPlugin = new JMenuItem("Load Plugin From Resources");
        plugins.add(loadPlugin);
        loadPlugin.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		JFileChooser fileChooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("JAR","jar");
				fileChooser.setFileFilter(filter);
				int selection = fileChooser.showOpenDialog(parent);
				if(selection == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					controller.loadPluginFromResources(file.getPath());
				}else if(selection == JFileChooser.ERROR_OPTION) {
					showMessage("Error while saving hosts");
				}
        	}
        });

        JMenu about = new JMenu("About");
        about.setMnemonic('A');
        JMenuItem help =new JMenuItem("Help"); 
        about.add(help);
        help.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		JPanel panel = PdfReader.openHelpPDF();
        		if(panel!=null) {
        			JDialog dialog = new JDialog(parent);
        			dialog.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
        			dialog.add(panel);     			
        			dialog.setResizable(false);
        			dialog.pack();
        			dialog.setVisible(true);
        		}else {
        			showMessage("No help available");
        		}
        			
        		
        		
        	}
        });
        
        
        
        about.addSeparator();
        about.add(new JMenuItem("About"));

        menuBar.add(file);
        menuBar.add(hosts);
        menuBar.add(plugins);
        menuBar.add(about);

        this.menuBar = menuBar;
        return menuBar;
    }

    /*
       anadir menu popup en JTree : basado  en https://stackoverflow.com/questions/517704/right-click-context-menu-for-java-jtree
    */
    private JTree createHostsTree() {
        JTree tree = new JTree();
        tree.setLargeModel(true);
        tree.setShowsRootHandles(true);
        tree.setRootVisible(false);
        tree.setEditable(false);

        ImageIcon serverIcon = new ImageIcon(MainWindow.class.getResource("/icons8-server-32-color.png"));
        ImageIcon containerIcon = new ImageIcon(MainWindow.class.getResource("/icons8-container-32.png"));
        DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
        renderer.setLeafIcon(containerIcon);
        renderer.setOpenIcon(serverIcon);
        renderer.setClosedIcon(serverIcon);
        tree.setCellRenderer(renderer);

        MouseAdapter ma = new MouseAdapter() {
            private void popupEvent(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                JTree tree = (JTree) e.getSource();
                TreePath path = tree.getPathForLocation(x, y);
                if (path == null) {
                	return;
                }
                
                tree.setSelectionPath(path);

                Object obj = path.getLastPathComponent();

                JPopupMenu popup = null;
                if (obj instanceof HostTreeModel.HostNode) {
                	HostTreeModel.HostNode hostNode = (HostTreeModel.HostNode) obj;
                	popup = createHostPopupMenu(hostNode);
                	
                } else if (obj instanceof HostTreeModel.ContainerNode) {
                	HostTreeModel.ContainerNode containerNode = (HostTreeModel.ContainerNode) obj;
                    if ((containerNode.getPlugins() != null) && (!containerNode.getPlugins().isEmpty()) && !containerNode.getContainer().equals(Host.NULLCONTAINER)) {
                        popup = createContainerPopupMenu(containerNode);
                    }
                }

                if (popup != null) {
                    popup.show(tree, x, y);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    popupEvent(e);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    popupEvent(e);
                }
            }
        };
        tree.addMouseListener(ma);

        HostTreeModel model = new HostTreeModel();
        tree.setModel(model);

        this.hostsTreeModel = model;
        this.hostsTree = tree;
        return tree;
    }

    private JPopupMenu createHostPopupMenu(HostTreeModel.HostNode hostNode) {
        JPopupMenu popup = new JPopupMenu();
        
        
        MainWindow parent = this;
        JMenuItem edit = new JMenuItem("Edit");
        popup.add(edit);
        edit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new HostManagementDialog(parent,controller,"Edit host",hostNode.getHost());
        		dialog.setVisible(true);
				
			}
        	
        });
        
        JMenuItem remove = new JMenuItem("Remove");
        popup.add(remove);  // No será un pluing, será una accion normal
        remove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Host host = hostNode.getHost();			
				controller.deleteHost(host);
			}	
        });
        
        
        
        
        
        JMenuItem connect = new JMenuItem("Connect");
        popup.add(connect);  // No será un pluing, será una accion normal
        connect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Host host = hostNode.getHost();
				Driver driver = controller.getAvailableDriverMap().get(host.getDriverName());
				host.setHostConnection(driver.connect(host));
				host.setContainers(driver.getContainers(host));
				controller.updateHostTree();
			}	
        });
        
        JMenuItem disconnect = new JMenuItem("Disconnect");
        popup.add(disconnect); // No será un pluing, será una accion normal
        disconnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Host host = hostNode.getHost();
				controller.getAvailableDriverMap().get(host.getDriverName()).disconnect(host);
				host.setNullContainer();
				controller.updateHostTree();
			}
        	
        });
        
        JMenuItem refresh = new JMenuItem("Refresh Containers");
        popup.add(refresh); 
        refresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Host host = hostNode.getHost();
				if(host.getHostConnection()!=null) {
					Driver driver = controller.getAvailableDriverMap().get(host.getDriverName());
					host.setContainers(driver.getContainers(host));
					controller.updateHostTree();
				}else {
					showMessage("Selected host is not connected, operation couldn't be executed");
				}
			}
        	
        });
        
        if ((hostNode.getPlugins() != null) && (!hostNode.getPlugins().isEmpty())) {
        	popup.addSeparator();
        	for (HostPlugin plugin : hostNode.getPlugins()) {
        		JMenuItem item = new JMenuItem(plugin.getInfo().getMenuLabel());
        		item.setToolTipText(plugin.getInfo().getDescription());
        		item.addActionListener((ActionEvent ae) -> {
        			PluginView panel = plugin.applyTo(hostNode.getHost(),controller);
        			controller.addAsociatedPanel(hostNode.getHost(), panel);
        			addPluginViewToTab(panel);
        		});
        		popup.add(item);
        	}            
        }    
        return popup;
    }

    private JPopupMenu createContainerPopupMenu(HostTreeModel.ContainerNode containerNode) {
        JPopupMenu popup = new JPopupMenu();
        for (ContainerPlugin plugin : containerNode.getPlugins()) {
            JMenuItem item = new JMenuItem(plugin.getInfo().getMenuLabel());
            item.setToolTipText(plugin.getInfo().getDescription());
            item.addActionListener((ActionEvent ae) -> {
            	PluginView panel = plugin.applyTo(containerNode.getContainer(),controller);
            	Host owner = containerNode.getContainer().getOwnerHost();           	
            	controller.addAsociatedPanel(owner, panel);
                panel.setOwner(owner);
            	addPluginViewToTab(panel);
            });
            popup.add(item);
        }
        return popup;
    }

    /*
        Anadir boton cerrar en tab :  https://stackoverflow.com/questions/517704/right-click-context-menu-for-java-jtree
     */
    
    public void addViewToTab(JPanel panel) {
    	panel.setOpaque(false);
         tabbedPane.add(panel);
         tabbedPane.setSelectedComponent(panel);
    }
    public void addPluginViewToTab(PluginView pluginPanel) {
        pluginPanel.setOpaque(false);
        tabbedPane.add(pluginPanel);

        JPanel titlePanel = createTitlePanel(pluginPanel);

        tabbedPane.setTabComponentAt(tabbedPane.indexOfComponent(pluginPanel), titlePanel);
        tabbedPane.setSelectedComponent(pluginPanel);
    }
    public void removePluginViewFromTab(PluginView pluginPanel) {
        pluginPanel.setOpaque(false);
        tabbedPane.remove(pluginPanel);
    }
    


    private JPanel createTitlePanel(PluginView pluginPanel) {
        JPanel titlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        titlePanel.setOpaque(false);

        // Create title panel with close button
        JLabel titleLbl = new JLabel(pluginPanel.getTitle());
        titleLbl.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        titlePanel.add(titleLbl);
        JButton closeButton = new JButton("x");
//        closeButton.setFont(closeButton.getFont().deriveFont(10f));
        closeButton.setMargin(new Insets(0, 0, 0, 0));
        closeButton.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {             
            	pluginPanel.closeView();              
                tabbedPane.remove(pluginPanel);
            }
        });
        titlePanel.add(closeButton);
        return titlePanel;
    }

    public void updateHostTreeModel(List<HostTreeModel.HostNode> hostNodes) {
        this.hostsTree.setModel(null);
        this.hostsTreeModel.setHostNodes(hostNodes);
        this.hostsTree.setModel(hostsTreeModel);
        this.expandHostTreeNodes();
    }

    public void clearHostTreeModel() {
        this.hostsTree.setModel(null);
        this.hostsTreeModel.clearNodes();
        this.hostsTree.setModel(hostsTreeModel);
        this.expandHostTreeNodes();
    }

    private JTabbedPane createTabbedPane() {
        JTabbedPane tabbedPane = new JTabbedPane();

        this.tabbedPane = tabbedPane;
        return tabbedPane;
    }

    private void expandHostTreeNodes() {
        for (int i = 0; i < hostsTree.getRowCount(); i++) {
            hostsTree.expandRow(i);
        }
    }
    
	public void showLoadedPlugins() {
		JDialog dialog = new JDialog(this);
		dialog.setLayout(new MigLayout());
		dialog.setResizable(false);
		dialog.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
		dialog.setLocationRelativeTo(this);
		Font font = new Font("Courier", Font.BOLD,12);
		JLabel hostLabel = new JLabel("Loaded Host Plugins:");
		hostLabel.setFont(font);
		dialog.add(hostLabel,"span");
		Iterator<HostPlugin> itH = controller.getAvailableHostPlugins().iterator();
		while(itH.hasNext()) {
			PluginInfo current = itH.next().getInfo();
			JLabel text = new JLabel(current.getName());
			text.setToolTipText(current.getDescription());
			dialog.add(text,"gapleft 30, span");
		}
		
		JLabel containerLabel = new JLabel("Loaded Container Plugins:");
		containerLabel.setFont(font);
		dialog.add(containerLabel,"span");
		
		Iterator<ContainerPlugin> itC = controller.getAvailableContainerPlugins().iterator();
		while(itC.hasNext()) {
			PluginInfo current = itC.next().getInfo();
			JLabel text = new JLabel(current.getName());
			text.setToolTipText(current.getDescription());
			dialog.add(text,"gapleft 30, span");
		}
		JLabel driverLabel = new JLabel("Loaded Drivers:");
		driverLabel.setFont(font);
		dialog.add(driverLabel,"span");
		Iterator<Driver> itD  = controller.getAvailableDrivers().iterator();
		while(itD.hasNext()) {
			DriverInfo current = itD.next().getDriverInfo();
			JLabel text = new JLabel(current.getName());
			text.setToolTipText(current.getDescription());
			dialog.add(text,"gapleft 30, span");
		}
		
		dialog.pack();
		dialog.setVisible(true);
	}
	

    
    public void showMessage(String text) {
    	JDialog dialog = new JDialog(this,"System message");
    	dialog.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
    	dialog.setResizable(false);
    	dialog.setLocationRelativeTo(this);
    	dialog.setLayout(new MigLayout());
    	Dimension size = new Dimension(100,100);
    	dialog.setMinimumSize(size);
    	JLabel message = new JLabel(text);
    	JButton ok = new JButton("Ok");
    	dialog.add(message,"span");
    	dialog.add(ok,"span");
    	dialog.pack();
    	ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.dispose();			
			}  		
    	});
    	dialog.setVisible(true);
    }

}
