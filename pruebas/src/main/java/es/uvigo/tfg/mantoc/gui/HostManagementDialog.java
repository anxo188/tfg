package es.uvigo.tfg.mantoc.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.NumberFormatter;

import es.uvigo.tfg.mantoc.core.Driver;
import es.uvigo.tfg.mantoc.core.Host;
import net.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class HostManagementDialog extends JDialog {

	MainController controller;
	MainWindow window;

	JLabel hostNameLabel;
	JLabel hostDescriptionLabel;
	JLabel hostDirectionLabel;
	JLabel hostPortLabel;
	JLabel hostTypeLabel;

	JTextField hostName;
	JTextArea hostDescription;
	JTextField hostDirection;
	JFormattedTextField hostPort;
	JComboBox<String> hostTypeSelector;
	
	Map<String, String> additionalParametersMap;
	JTable additionalParameters;
	DefaultTableModel additionalParametersModel;

	JButton addButton;
	JButton cancel;
	JButton addParameter;
	JDialog current;
	JButton deleteParameters;
	
	String originalName;
	boolean edit;
	Host host;

	public HostManagementDialog(MainWindow window, MainController controller, String text) {
		super(window, text, true);
		this.controller = controller;
		this.window = window;
		this.edit = false;
		this.setResizable(false);
		this.host=new Host();
		this.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
		Dimension size = new Dimension(100, 100);
		this.setMinimumSize(size);
		this.setLocationRelativeTo(window);
		MigLayout layout = new MigLayout();
		this.setLayout(layout);

		this.initialiceComponents();

		additionalParametersModel = new DefaultTableModel();
		additionalParametersModel.addColumn("Parameter");
		additionalParametersModel.addColumn("Value");
		this.additionalParameters = new JTable(additionalParametersModel);
		this.additionalParameters.setShowGrid(true);

		this.setListeners();
		this.addToDialog();
		this.pack();

	}
	public HostManagementDialog(MainWindow window, MainController controller, String text,Host host) {
		super(window, text, true);
		this.controller = controller;
		this.window = window;
		this.host = host;
		this.originalName = host.getName();
		this.edit=true;
		this.setResizable(false);
		this.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
		Dimension size = new Dimension(100, 100);
		this.setMinimumSize(size);
		this.setLocationRelativeTo(window);
		MigLayout layout = new MigLayout();
		this.setLayout(layout);

		this.initialiceComponents();

		additionalParametersModel = new DefaultTableModel();
		additionalParametersModel.addColumn("Parameter");
		additionalParametersModel.addColumn("Value");
		this.additionalParameters = new JTable(additionalParametersModel);
		this.additionalParameters.setShowGrid(true);

		this.setListeners();
		this.loadValues();
		this.addToDialog();
		this.pack();

	}
	

	private void initialiceComponents() {
		hostNameLabel = new JLabel("Host Alias: ");
		hostDescriptionLabel = new JLabel("Host Description: ");
		hostDirectionLabel = new JLabel("Host Direction: ");
		hostPortLabel = new JLabel("Host Port: ");
		hostTypeLabel = new JLabel("Host Type: ");
		hostName = new JTextField(15);
		hostDescription = new JTextArea(10, 50);
		hostDirection = new JTextField(50);
		hostTypeSelector = new JComboBox<String>();
		additionalParametersMap = new HashMap<String, String>();
		if(!edit) {
			addButton = new JButton("Confirm and add");
		}else {
			addButton = new JButton("Confirm and edit");
		}
		cancel = new JButton("Cancel");
		addParameter = new JButton("Add new Parameter");
		deleteParameters = new JButton("Delete selected Parameters");

		NumberFormat format = NumberFormat.getInstance();
		format.setGroupingUsed(false);
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(0);
		formatter.setMaximum(65535);
		formatter.setAllowsInvalid(false);
		this.hostPort = new JFormattedTextField(formatter);
		this.hostDescription.setLineWrap(true);

		List<Driver> drivers = controller.getAvailableDrivers();
		if (drivers.isEmpty()) {
			hostTypeSelector.addItem("No drivers availables");
		}
		Iterator<Driver> it = drivers.iterator();
		while (it.hasNext()) {
			Driver current = it.next();
			hostTypeSelector.addItem(current.getDriverInfo().getName());
		}

	}

	private void addToDialog() {
		this.add(hostNameLabel, "span");
		this.add(hostName, "span");
		this.add(hostDescriptionLabel, "span");
		this.add(hostDescription, "span");
		this.add(hostDirectionLabel, "span");
		this.add(hostDirection, "span");
		this.add(hostPortLabel, "span");
		this.add(hostPort, "span");
		this.add(hostTypeLabel, "span");
		this.add(hostTypeSelector, "span");
		JScrollPane wrapTable = new JScrollPane(additionalParameters);
		wrapTable.setMaximumSize(new Dimension(700, 300));
		this.add(wrapTable, "span");
		this.add(this.addParameter);
		this.add(this.deleteParameters, "span");
		this.add(addButton);
		this.add(cancel);
	}

	private void loadValues() {
		hostName.setText(this.host.getName());
		hostDirection.setText(this.host.getDirection());
		hostPort.setValue(Integer.parseInt(this.host.getPort()));
		hostDescription.setText(this.host.getDescription());
		hostTypeSelector.setSelectedItem(this.host.getDriverName());
		this.additionalParametersMap = this.host.getAdditionalParameters();
		
		Set<String> setKeys = this.additionalParametersMap.keySet();
		Iterator<String> it = setKeys.iterator();
		while(it.hasNext()) {
			String key = it.next();
			String[] par = { key, additionalParametersMap.get(key) };
			additionalParametersModel.insertRow(0, par);
		}
		
	}

	private void setListeners() {

		this.deleteParameters.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int[] rows = additionalParameters.getSelectedRows();
				for (int i = 0; i < rows.length; i++) {
					additionalParametersMap.remove(additionalParametersModel.getValueAt(i, 0));
					additionalParametersModel.removeRow(rows[i] - i);
				}

			}
		});

		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				Host test = new Host();
				test.setName(hostName.getText());
				if(controller.getHosts().contains(test) && edit && !hostName.getText().contentEquals(originalName)) {
					window.showMessage("Host alias must be unique");
				}else {
					host.setNullContainer();
					if (!additionalParametersMap.isEmpty())
						host.setAdditionalParameters(additionalParametersMap);
					
					host.setName(hostName.getText());
					host.setDriverName(hostTypeSelector.getSelectedItem().toString());
					host.setDirection(hostDirection.getText());
					host.setPort(hostPort.getText());
					host.setDescription(hostDescription.getText());
					if(!edit)
						controller.addHost(host);
					if(edit) {
						
						host.setHostConnection(null);
						controller.updateHostTree();
					}
					dispose();				
				}
				
			}
		});

		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		this.addParameter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog insertParams = new JDialog(window, "Insert Additional Parameters");
				insertParams.setModalityType(JDialog.ModalityType.APPLICATION_MODAL);
				insertParams.setLocationRelativeTo(window);

				MigLayout layout = new MigLayout();
				insertParams.setLayout(layout);

				JLabel paramLabel = new JLabel("Parameter: ");
				JLabel valueLabel = new JLabel("Value: ");
				JTextField param = new JTextField();
				JTextField value = new JTextField();

				insertParams.add(paramLabel);
				insertParams.add(param, "span");
				insertParams.add(valueLabel);
				insertParams.add(value, "span");

				JButton addButton = new JButton("Add");
				JButton back = new JButton("Cancel");
				insertParams.add(addButton);
				insertParams.add(back);
				addButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						additionalParametersMap.put(param.getText(), value.getText());
						String[] par = { param.getText(), value.getText() };
						additionalParametersModel.insertRow(0, par);
						insertParams.dispose();

					}

				});
				back.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						insertParams.dispose();

					}

				});

				insertParams.pack();
				insertParams.setVisible(true);
			}

		});
	}

}
