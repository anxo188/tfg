package es.uvigo.tfg.mantoc.plugins;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.pf4j.DefaultPluginManager;
import org.pf4j.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.uvigo.tfg.mantoc.core.Driver;

public class MainPluginManager {
	 	final Logger logger = LoggerFactory.getLogger(MainPluginManager.class);
	    
	    private List<HostPlugin> availableHostPlugins;
	    private List<ContainerPlugin> availableContainerPlugins;
	    private List<Driver> availableDriversList;
	    private Map<String,Driver> availableDriversMap;
	    
	    public MainPluginManager() {
	        this.availableHostPlugins = new ArrayList<>();
	        this.availableContainerPlugins = new ArrayList<>();
	        this.availableDriversMap = new HashMap<>();
	        this.availableDriversList = new ArrayList<>();
	    }

	    public List<HostPlugin> getAvailableHostPlugins() {
	        return this.availableHostPlugins;
	    }
	    public List<ContainerPlugin> getAvailableContainerPlugins() {
	        return this.availableContainerPlugins;
	    }
	    public Map<String,Driver> getAvailableDriversMap() {
	        return this.availableDriversMap;
	    }
	    public  List<Driver> getAvailableDriversList() {
	        return this.availableDriversList;
	    }
	    

	    public void loadCorePlugins() {

	    	PluginManager pluginManager = new DefaultPluginManager();
	        pluginManager.loadPlugins();
	        pluginManager.startPlugins();
	       
	        this.availableHostPlugins.addAll(pluginManager.getExtensions(HostPlugin.class));  
	        logger.info("Loaded "+this.availableHostPlugins.size()+" core host plugins");
	        
	        this.availableContainerPlugins.addAll(pluginManager.getExtensions(ContainerPlugin.class));  
	        logger.info("Loaded "+this.availableContainerPlugins.size()+" core container plugins");   
	        
	        availableDriversList.addAll(pluginManager.getExtensions(Driver.class));  
	        logger.info("Loaded "+this.availableDriversList.size()+" driver plugins");
	
	        Iterator<Driver> it = availableDriversList.iterator();
	        while(it.hasNext()) {
	        	Driver current = it.next();
	        	this.availableDriversMap.put(current.getDriverInfo().getName(),current);
	        }
	        
	    }
	    public List<Driver> loadDrivers() {
	    	PluginManager pluginManager = new DefaultPluginManager();
	        pluginManager.loadPlugins();
	        pluginManager.startPlugins();
	        return pluginManager.getExtensions(Driver.class);  
		       
	    }
	    
	    public void loadPluingsFromResource(String resourceURI) {
	    	 PluginManager pm = new DefaultPluginManager();
             pm.loadPlugin(Paths.get(resourceURI));
             pm.startPlugins();
             
             
	        for (HostPlugin hp : pm.getExtensions(HostPlugin.class)) {
	        	if(!availableHostPlugins.contains(hp))
	        		availableHostPlugins.add(hp);
             }
                             
	        for (ContainerPlugin cp : pm.getExtensions(ContainerPlugin.class)) {
	        	if(!availableContainerPlugins.contains(cp))
	        		availableContainerPlugins.add(cp);
             }
	        
	        for (Driver dv : pm.getExtensions(Driver.class)) {
	        	if(!availableDriversList.contains(dv)) { 	
	        		availableDriversList.add(dv);
	        		availableDriversMap.put(dv.getDriverInfo().getName(),dv);
	        	}
	        		
             }
	    }
	    
	    public void loadPluingsFromDefaultResources() {
	    	String resourceURI = "./plugins";
	    			Path path = Paths.get(resourceURI);
	    	if(path ==null)
	    		return;
	    	PluginManager pm = new DefaultPluginManager();
            pm.loadPlugin(path);
            pm.startPlugins();
            
            
	        for (HostPlugin hp : pm.getExtensions(HostPlugin.class)) {
	        	
	        	boolean exist = false;
	        	Iterator<HostPlugin> it = availableHostPlugins.iterator();
	        	while(it.hasNext() && exist == false) {
	        		if( it.next().getInfo().getName().equals(hp.getInfo().getName()) )
	        			exist = true;
	        	}
	        	if(!exist)
	        		availableHostPlugins.add(hp);
            }
                            
	        for (ContainerPlugin cp : pm.getExtensions(ContainerPlugin.class)) {
	        	        	
	        	boolean exist = false;
	        	Iterator<ContainerPlugin> it = availableContainerPlugins.iterator();
	        	while(it.hasNext() && exist == false) {
	        		if( it.next().getInfo().getName().equals(cp.getInfo().getName()) )
	        			exist = true;
	        	}
	        	if(!exist)
	        		availableContainerPlugins.add(cp);
            }
	        
	        for (Driver dv : pm.getExtensions(Driver.class)) {
	        
	        	boolean exist = false;
	        	Iterator<Driver> it = availableDriversList.iterator();
	        	while(it.hasNext() && exist == false) {
	        		if( it.next().getDriverInfo().getName().equals(dv.getDriverInfo().getName()) )
	        			exist = true;
	        	}
	        	if(!exist) { 	
	        		availableDriversList.add(dv);
	        		availableDriversMap.put(dv.getDriverInfo().getName(),dv);
	        	}
	        		
            }
	    }
}
