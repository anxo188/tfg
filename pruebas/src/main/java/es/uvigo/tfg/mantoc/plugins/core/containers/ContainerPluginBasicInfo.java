package es.uvigo.tfg.mantoc.plugins.core.containers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.pf4j.Extension;

import com.github.dockerjava.api.command.InspectContainerResponse;

import es.uvigo.tfg.mantoc.core.Container;
import es.uvigo.tfg.mantoc.core.ContainerInfo;
import es.uvigo.tfg.mantoc.core.ExposedPort;
import es.uvigo.tfg.mantoc.docker.DockerContainer;
import es.uvigo.tfg.mantoc.docker.DockerContainerInfo;
import es.uvigo.tfg.mantoc.gui.PluginViewCloseListener;
import es.uvigo.tfg.mantoc.plugins.ContainerPlugin;
import es.uvigo.tfg.mantoc.plugins.PluginInfo;
import es.uvigo.tfg.mantoc.plugins.PluginView;
import net.miginfocom.swing.MigLayout;

@Extension
public class ContainerPluginBasicInfo implements ContainerPlugin{
	 
	JTextField dataTime;
	JTextField platform;
	 JTextField volumes;
	 JTextField tty;
	  JTextField imageId;
	  JTextField createdDate;
	  JTextField imageName;
	  JTextField status;
	@Override
	public PluginInfo getInfo() {
		return new PluginInfo("Container Basic Info","Plugin for container info visualization","Info");
	}

	@Override
	public boolean isApplicableTo(Container container) {
		return true;
	}

	@Override
	public PluginView applyTo(Container container,PluginViewCloseListener listener) {

		
		ContainerInfo info  = container.getOwnerHost().getHostConnection().getDriver().getContainerInfo(container);
		
        PluginView panel = new PluginView(this.getInfo().getName()+" of " + container.getName());
        panel.setLayout(new MigLayout());
        panel.setPluginCloseListener(listener);
        panel.setOwner(container.getOwnerHost());
       
        
        panel.add(new JLabel("Container id"));
        JTextField containerId = new JTextField(info.getId());
        containerId.setEditable(false);
        panel.add(containerId, "span");
        
        panel.add(new JLabel("Status:"));
        status = new JTextField();
        status.setText(info.getStatus());
        status.setEditable(false);
        panel.add(status, "span");
       
        panel.add(new JLabel("Exposed Ports:"),"span");
        List<ExposedPort> exposedPorts = info.getExposedPorts();
        Iterator<ExposedPort> it = exposedPorts.iterator();
        while(it.hasNext()) {
        	JTextField exposedPort = new JTextField(it.next().toString());
        	exposedPort.setEditable(false);
        	panel.add(exposedPort, "span");	
        }
        if(container instanceof DockerContainer) {
        	DockerContainerInfo infoDocker  = (DockerContainerInfo) info;
        	InspectContainerResponse inspector = infoDocker.getInspector();
        	panel.add(new JLabel("Created date:"));
        	createdDate =new JTextField();
        	createdDate.setText(inspector.getCreated());
        	createdDate.setEditable(false);
        	panel.add(createdDate, "span");
        	
        	panel.add(new JLabel("Image name:"));
        	imageName = new JTextField();
        	imageName.setText(infoDocker.getImageName());
        	imageName.setEditable(false);
        	panel.add(imageName, "span");
        	
        	panel.add(new JLabel("Image id:"));
        	imageId = new JTextField();
        	imageId.setText(inspector.getImageId());
        	imageId.setEditable(false);
        	panel.add(imageId, "span");
        	
        	
        	panel.add(new JLabel("TTY enabled:"));
        	tty = new JTextField();
        	if(inspector.getConfig().getTty()) {
        		tty.setText("yes");  		
        	}else {
        		tty.setText("no");
        	}
        	tty.setEditable(false);
        	panel.add(tty, "span");
        	
        	
        	panel.add(new JLabel("Docker driver:"));
        	JTextField dockerDriver = new JTextField(inspector.getDriver());
        	dockerDriver.setEditable(false);
        	panel.add(dockerDriver, "span");
        	
        	panel.add(new JLabel("Volumes:"));
        	if(inspector.getVolumes() != null) {
        		volumes = new JTextField(inspector.getVolumes().toString());
        	}else {
        		volumes = new JTextField("No volume detected");
        	}
        	volumes.setEditable(false);
        	panel.add(volumes, "span");
        	
        	panel.add(new JLabel("Platform: "));
        	platform =new JTextField() ;
        	
        	platform.setText(inspector.getPlatform());
        	platform.setEditable(false);
        	panel.add(platform, "span");
        }
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
        LocalDateTime now = LocalDateTime.now();  
        dataTime = new JTextField();
        dataTime.setText(dtf.format(now));
        dataTime.setEditable(false);
        panel.add(new JLabel("Data time:"));
        panel.add(dataTime,"span");
        
        JButton refresh = new JButton("Refresh Data");
        refresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateData(container);
				
			}
        	
        });
        panel.add(refresh);
        
		return panel;
	}
	
	public void updateData(Container container) {
		DockerContainerInfo info  = (DockerContainerInfo) container.getOwnerHost().getHostConnection().getDriver().getContainerInfo(container);
		InspectContainerResponse inspector = info.getInspector();
		
		status.setText(info.getStatus());
		status.repaint();	
		if(container instanceof DockerContainer) {
			if(inspector.getConfig().getTty()) {
	    		tty.setText("yes");  		
	    	}else {
	    		tty.setText("no");
	    	}
			tty.repaint();
			if(inspector.getVolumes() != null) {
	        	volumes = new JTextField(inspector.getVolumes().toString());
	    	}else {
	    		volumes = new JTextField("No volume detected");
	    	}
			volumes.repaint();
		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
	    LocalDateTime now = LocalDateTime.now();  
		dataTime.setText(dtf.format(now));
		dataTime.repaint();
	}

}



