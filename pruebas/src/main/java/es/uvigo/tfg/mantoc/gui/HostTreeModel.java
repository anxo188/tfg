package es.uvigo.tfg.mantoc.gui;

import es.uvigo.tfg.mantoc.core.Container;
import es.uvigo.tfg.mantoc.core.Host;
import es.uvigo.tfg.mantoc.plugins.ContainerPlugin;
import es.uvigo.tfg.mantoc.plugins.HostPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class HostTreeModel implements TreeModel {

    private List<HostNode> hostNodes = new ArrayList<>();

    private List<TreeModelListener> listeners = new ArrayList<>();

    public void setHostNodes(List<HostNode> hostNodes) {
        this.hostNodes = hostNodes;
    }

    
    
    public void addHost(Host host) {
        this.hostNodes.add(new HostNode(host));
    }

    public void addHost(Host host, List<ContainerNode> containerNodes, List<HostPlugin> plugins) {
        this.hostNodes.add(new HostNode(host, containerNodes, plugins));
    }

    public void removeHost(Host host) {
        HostNode hostNode = new HostNode(host);
        this.removeHostNode(hostNode);
    }

    public void removeHostNode(HostNode hostNode) {
        this.hostNodes.remove(hostNode);
    }

    public void clearNodes() {
        this.hostNodes.clear();
    }
       
    @Override
    public Object getRoot() {
        return "Hosts";
    }

    @Override
    public Object getChild(Object o, int i) {
        if (o instanceof String) {
            // At root
            return this.hostNodes.get(i);
        }

        if (o instanceof HostNode) {
            HostNode hostNode = (HostNode) o;
            if ((hostNode.containerNodes != null) && (!hostNode.containerNodes.isEmpty())) {
                return hostNode.containerNodes.get(i);
            }
        }
        return null;
    }

    @Override
    public int getChildCount(Object o) {
        if (o instanceof String) {
            // At root
            return this.hostNodes.size();
        }
        if (o instanceof HostNode) {
            HostNode hostNode = (HostNode) o;
            if (hostNode.containerNodes != null) {
                return hostNode.containerNodes.size();
            }
        }
        return 0;
    }

    @Override
    public boolean isLeaf(Object o) {
        return (this.getChildCount(o) == 0);
    }

    @Override
    public void valueForPathChanged(TreePath tp, Object o) {
        // Not applicable
    }

    @Override
    public int getIndexOfChild(Object o, Object o1) {
        if (o instanceof String) {
            // At root
            return this.hostNodes.indexOf(o1);
        }
        if (o instanceof HostNode) {
            HostNode hostNode = (HostNode) o;
            if ((hostNode.containerNodes != null) && (o1 != null) && (o1 instanceof ContainerNode)) {
                ContainerNode containerNode = (ContainerNode) o1;
                return hostNode.containerNodes.indexOf(containerNode);
            }
        }
        return -1;
    }

    @Override
    public void addTreeModelListener(TreeModelListener tl) {
        this.listeners.add(tl);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener tl) {
        this.listeners.remove(tl);
    }

    public static class HostNode {

        private String label;
        private Host host;
        private List<HostPlugin> plugins;
        private List<ContainerNode> containerNodes;

        public HostNode(Host host) {
            this.host = host;
            this.label = host.getName();
            if (host.getContainers() != null) {
                this.containerNodes = new ArrayList<>();
                for (Container container : host.getContainers()) {
                    this.containerNodes.add(new ContainerNode(container));
                }
            }
        }

        public HostNode(Host host, List<ContainerNode> containerNodes, List<HostPlugin> plugins) {
            this.host = host;
            this.label = host.getName();
            this.containerNodes = containerNodes;
            this.plugins = plugins;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Host getHost() {
            return host;
        }

        public void setHost(Host host) {
            this.host = host;
        }

        public List<HostPlugin> getPlugins() {
            return plugins;
        }

        public void setPlugins(List<HostPlugin> plugins) {
            this.plugins = plugins;
        }

        public List<ContainerNode> getContainerNodes() {
            return containerNodes;
        }

        public void setContainerNodes(List<ContainerNode> containerNodes) {
            this.containerNodes = containerNodes;
        }

        @Override
        public String toString() {
            return label;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 59 * hash + Objects.hashCode(this.host);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final HostNode other = (HostNode) obj;
            if (!Objects.equals(this.host, other.host)) {
                return false;
            }
            return true;
        }

    }

    public static class ContainerNode {

        private String label;
        private Container container;
        private List<ContainerPlugin> plugins;

        public ContainerNode(Container container) {
            this.label = container.getName();
            this.container = container;
        }

        public ContainerNode(Container container, List<ContainerPlugin> plugins) {
            this.label = container.getName();            
            this.container = container;
            this.plugins = plugins;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Container getContainer() {
            return container;
        }

        public void setContainer(Container container) {
            this.container = container;
        }

        public List<ContainerPlugin> getPlugins() {
            return plugins;
        }

        public void setPlugins(List<ContainerPlugin> plugins) {
            this.plugins = plugins;
        }

        @Override
        public String toString() {
            return label;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 73 * hash + Objects.hashCode(this.container);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ContainerNode other = (ContainerNode) obj;
            if (!Objects.equals(this.container, other.container)) {
                return false;
            }
            return true;
        }

    }

}
