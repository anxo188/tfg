package es.uvigo.tfg.mantoc.docker;

import es.uvigo.tfg.mantoc.core.Container;
import es.uvigo.tfg.mantoc.core.Host;

public class DockerContainer extends Container{
	
	private com.github.dockerjava.api.model.Container origin;
	
	public DockerContainer(com.github.dockerjava.api.model.Container container,Host host) {
		super();
		this.origin = container;
		this.id = container.getId();
		this.ownerHost = host;
		this.name = container.getNames()[0];
		this.description = container.getImage();
	}
	
	public com.github.dockerjava.api.model.Container getApiContainer(){
		return this.origin;
	}

}
