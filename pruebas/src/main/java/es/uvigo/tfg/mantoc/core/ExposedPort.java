package es.uvigo.tfg.mantoc.core;

import java.util.ArrayList;
import java.util.List;

public class ExposedPort {
	private String internalPort;
	private String externalPort;
	private List<String> protocols;
	
	public ExposedPort(String internal, String external,List<String> protocols) {
		this.internalPort = internal;
		this.externalPort = external;
		this.protocols = protocols;
	}
	public ExposedPort(String internal, String external,String protocols) {
		this.internalPort = internal;
		this.externalPort = external;
		this.protocols=new ArrayList<String>();
		this.protocols.add(protocols);
	}
	
	public String getExternalPort() {
		return externalPort;
	}

	public void setExternalPort(String externalPort) {
		this.externalPort = externalPort;
	}

	public List<String> getProtocols() {
		return protocols;
	}

	public void setProtocols(List<String> protocols) {
		this.protocols = protocols;
	}

	public void setInternalPort(String internalPort) {
		this.internalPort = internalPort;
	}

	public String getInternalPort() {
		return this.internalPort;
	}
	@Override
	public String toString() {
		String result = "";
		
		if(externalPort == null)
			result = "?";
		else
			result = externalPort;
		
		if(internalPort == null)
			result = result+":?";
		else
			result = result+":"+internalPort;
		if(protocols == null || protocols.isEmpty())
			result = result+"()";
		else
			result = result+"("+protocols.toString()+")";
		
		
		return result;	
	}
}
