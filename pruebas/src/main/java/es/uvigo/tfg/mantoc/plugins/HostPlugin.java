/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.plugins;

import org.pf4j.ExtensionPoint;

import es.uvigo.tfg.mantoc.core.Host;
import es.uvigo.tfg.mantoc.gui.MainController;
import es.uvigo.tfg.mantoc.gui.PluginViewCloseListener;

/**
 *
 * @author anxo
 */
public interface HostPlugin extends ExtensionPoint {
    public PluginInfo getInfo();
    public boolean isApplicableTo(Host host);
    public PluginView applyTo(Host host,PluginViewCloseListener listener);
}
