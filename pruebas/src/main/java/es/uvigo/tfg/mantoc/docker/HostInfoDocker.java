package es.uvigo.tfg.mantoc.docker;

import com.github.dockerjava.api.model.Info;
import com.github.dockerjava.api.model.InfoRegistryConfig;

import es.uvigo.tfg.mantoc.core.HostInfo;

public class HostInfoDocker extends HostInfo{

	private String dockerRootDir;
	private String registryConfig;
	
	
	
	
	
	public HostInfoDocker(Info info) {
		this.architecture = info.getArchitecture();
		this.id = info.getId();
		this.memLimit = info.getMemoryLimit();
		this.memTotal = info.getMemTotal();
		this.nContainers = info.getContainers();
		this.nContainersPaused = info.getContainersPaused();
		this.nContainersRunning = info.getContainersRunning();
		this.nContainersStopped = info.getContainersStopped();
		this.nImages = info.getImages();
		this.dockerRootDir = info.getDockerRootDir();
		this.oS = info.getOperatingSystem();
		this.nCpu = info.getNCPU();
		this.serverVersion = info.getServerVersion();
		this.systemTime = info.getSystemTime();
		
		InfoRegistryConfig conf =info.getRegistryConfig();
		if(conf !=null)
			this.registryConfig = conf.toString();
		else
			this.registryConfig = "";
	}
	

    public String getDockerRootDir() {
		return dockerRootDir;
	}



	public void setDockerRootDir(String dockerRootDir) {
		this.dockerRootDir = dockerRootDir;
	}



	public String getRegistryConfig() {
		return registryConfig;
	}



	public void setRegistryConfig(String registryConfig) {
		this.registryConfig = registryConfig;
	}



	



}
