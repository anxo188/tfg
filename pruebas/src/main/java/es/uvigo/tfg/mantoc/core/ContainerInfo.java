package es.uvigo.tfg.mantoc.core;

import java.util.List;

public class ContainerInfo {
	protected String name;
	protected String id;
	protected String status;
	protected List<ExposedPort> exposedPorts;
	
	public ContainerInfo() {
		
	}
	
	public ContainerInfo(String name, String id,List<ExposedPort> exposedPorts, String status) {
		this.name = name;
		this.id = id;
		this.status = status;
		this.exposedPorts=exposedPorts;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}

	public List<ExposedPort> getExposedPorts() {
		return this.exposedPorts;
	}

	public void setExposedPorts(List<ExposedPort> exposedPorts) {
		this.exposedPorts = exposedPorts;
	}
}
