/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.plugins.core.hosts;

import es.uvigo.tfg.mantoc.core.Host;
import es.uvigo.tfg.mantoc.core.HostInfo;
import es.uvigo.tfg.mantoc.docker.HostInfoDocker;
import es.uvigo.tfg.mantoc.gui.PluginViewCloseListener;
import es.uvigo.tfg.mantoc.plugins.HostPlugin;
import es.uvigo.tfg.mantoc.plugins.PluginInfo;
import es.uvigo.tfg.mantoc.plugins.PluginView;
import net.miginfocom.swing.MigLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.pf4j.Extension;

/**
 *
 * @author anxo
 */
@Extension
public class HostPluginGeneral implements HostPlugin {

	JTextField dataTime;
	  JTextField nContRun;
      JTextField nContPau;
      JTextField nContStop;
      JTextField imageId;
      JTextField nCont;
      
    @Override
    public PluginInfo getInfo() {
        return new PluginInfo("Vista general", "Vista general", "Info");
    }

    @Override
    public boolean isApplicableTo(Host host) {
    
    	if(host.getHostConnection() != null) {
			return true;
		}else {			
			return false;
		}
    }

    @Override
    public PluginView applyTo(Host host,PluginViewCloseListener listener) {
    	HostInfo info  = host.getHostConnection().getDriver().getHostInfo(host);
    	
        PluginView panel = new PluginView(this.getInfo().getName()+" of " + host.getName());
        panel.setLayout(new MigLayout());
        panel.setPluginCloseListener(listener);
        panel.setOwner(host);
        panel.add(new JLabel("Host id"));
        JTextField containerId = new JTextField(info.getId());
        containerId.setEditable(false);
        panel.add(containerId, "span");
        
        panel.add(new JLabel("Host architecture:"));
        JTextField architecture = new JTextField(info.getArchitecture());
        architecture.setEditable(false);
        panel.add(architecture, "span");
        
        panel.add(new JLabel("Host OS:"));
        JTextField os = new JTextField(info.getoS());
        os.setEditable(false);
        panel.add(os, "span");

        panel.add(new JLabel("Detected Cpu:"));
        JTextField ncpu = new JTextField(Integer.toString(info.getnCpu()));
        ncpu.setEditable(false);
        panel.add(ncpu, "span");

        panel.add(new JLabel("Memory limit:"));
        JTextField memLimit = new JTextField();
        if(info.getMemLimit()) {
        	memLimit.setText("yes");  		
        }else {
        	memLimit.setText("no");
        }
        memLimit.setEditable(false);
        panel.add(memLimit, "span");
       
        panel.add(new JLabel("Detected Memory:"));
        JTextField mem = new JTextField(Long.toString(info.getMemTotal()));
        mem.setEditable(false);
        panel.add(mem, "span");
        
       
        panel.add(new JLabel("Containers detected:"));
        nCont = new JTextField(Integer.toString(info.getnContainers()));
        nCont.setEditable(false);
        panel.add(nCont, "span");
        
        panel.add(new JLabel("Running Containers detected:"));
        nContRun = new JTextField(Integer.toString(info.getnContainersRunning()));
        nContRun.setEditable(false);
        panel.add(nContRun, "span");

        panel.add(new JLabel("Paused Containers detected:"));
        nContPau = new JTextField(Integer.toString(info.getnContainersPaused()));
        nContPau.setEditable(false);
        panel.add(nContPau, "span");
        
        panel.add(new JLabel("Stopped Containers detected:"));
        nContStop = new JTextField(Integer.toString(info.getnContainersStopped()));
        nContStop.setEditable(false);
        panel.add(nContStop, "span");
        
        panel.add(new JLabel("Available Images Count:"));
        imageId = new JTextField(Integer.toString(info.getnImages()));
        imageId.setEditable(false);
        panel.add(imageId, "span");
        
        if(host.getDriverName().equals("Docker")) {
        	HostInfoDocker infoDocker = (HostInfoDocker) info;
        	panel.add(new JLabel("Docker version:"));
        	JTextField dVer = new JTextField(info.getServerVersion());
        	dVer.setEditable(false);
        	panel.add(dVer, "span");
        	
        	
        	panel.add(new JLabel("Docker Root Directory:"));
        	
        	JTextField dRDir = new JTextField(infoDocker.getDockerRootDir());
        	dRDir.setEditable(false);
        	panel.add(dRDir, "span");
        	
        	panel.add(new JLabel("Docker Registry Config:"));
        	JTextArea dRegistry = new JTextArea(infoDocker.getDockerRootDir());
        	dRegistry.setEditable(false);
        	panel.add(dRegistry, "span");     	
        }
        
        
        
        
        

        panel.add(new JLabel("Time in host system:"));
        JTextField time = new JTextField(info.getSystemTime());
        time.setEditable(false);
        panel.add(time, "span");
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
        LocalDateTime now = LocalDateTime.now();  
        dataTime = new JTextField();
        dataTime.setText(dtf.format(now));
        dataTime.setEditable(false);
        panel.add(new JLabel("Data time:"));
        panel.add(dataTime,"span");
        
        JButton refresh = new JButton("Refresh Data");
        refresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateData(host);
				
			}
        	
        });
        panel.add(refresh);
        
        
		return panel;     
    }
    
    public void updateData(Host host) {
    	HostInfo info  = host.getHostConnection().getDriver().getHostInfo(host);

         nCont.setText(Integer.toString(info.getnContainers()));
         nCont.repaint();
         nContRun.setText(Integer.toString(info.getnContainersRunning()));
         nContRun.repaint();
         nContPau.setText(Integer.toString(info.getnContainersPaused()));
         nContPau.repaint();
         nContStop.setText(Integer.toString(info.getnContainersStopped()));
         nContStop.repaint();
         imageId.setText(Integer.toString(info.getnImages()));
         imageId.repaint();
    	
    	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
	    LocalDateTime now = LocalDateTime.now();  
		dataTime.setText(dtf.format(now));
		dataTime.repaint();
    }
    
}
