/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc;

import com.formdev.flatlaf.FlatLightLaf;

import es.uvigo.tfg.mantoc.gui.MainController;
import es.uvigo.tfg.mantoc.gui.MainWindow;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author anxo
 * Mantoc
 */
public class Main {

    public final static void main(String[] args) {
        FlatLightLaf.install(new FlatLightLaf());   // estilo claro
        //FlatLightLaf.install(new FlatDarkLaf());   // estilo oscuro
           
  
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {

                MainController controller = new MainController();
                controller.loadCorePlugins();
                controller.loadHosts();

                MainWindow frame = new MainWindow(controller);

                frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                frame.addWindowListener(new WindowAdapter(){
                    public void windowClosing(WindowEvent e){
                        controller.saveHosts();                     
                        System.exit(0);
                    }
                });
                frame.setVisible(true);
            }
        });

    }


}
