/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.core;

import java.util.Objects;

/**
 *
 * @author anxo
 */
public class Container {
	protected String name;
    protected String description;
    protected String id;
    protected Host ownerHost;
    protected ContainerInfo info;
    public Container() {
    }

    public Container(String id, String name, String description) {
        this.name = name;
        this.description = description;
        this.ownerHost = null;
    }

    
    public Container(String id, String name, String description, Host ownerHost) {
        this.name = name;
        this.description = description;
        this.ownerHost = ownerHost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Host getOwnerHost() {
        return ownerHost;
    }

    public void setOwnerHost(Host ownerHost) {
        this.ownerHost = ownerHost;
    }
    
    public ContainerInfo getContainerInfo() {
    	return this.info;
    }
    public void setContainerInfo(ContainerInfo info) {
    	this.info=info;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Container other = (Container) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

	public String getId() {
		return id;
	}
    
    
    
}
