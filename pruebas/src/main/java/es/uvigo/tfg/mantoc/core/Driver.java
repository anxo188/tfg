/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.core;

import java.util.List;
import org.pf4j.ExtensionPoint;

/**
 *
 * @author anxo
 */
public interface Driver extends ExtensionPoint {
	DriverInfo getDriverInfo();
	HostConnection connect(Host host);
	boolean disconnect(Host host);
	List<Container> getContainers(Host host);
	ContainerInfo getContainerInfo(Container container);
	HostInfo getHostInfo(Host host);
	boolean stopContainer(Container container);
	boolean haltContainer(Container container);
	boolean resumeContainer(Container container);
}
