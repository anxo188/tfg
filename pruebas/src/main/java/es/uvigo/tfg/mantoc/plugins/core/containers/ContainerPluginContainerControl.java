package es.uvigo.tfg.mantoc.plugins.core.containers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.pf4j.Extension;

import es.uvigo.tfg.mantoc.core.Container;
import es.uvigo.tfg.mantoc.core.ContainerInfo;
import es.uvigo.tfg.mantoc.gui.PluginViewCloseListener;
import es.uvigo.tfg.mantoc.plugins.ContainerPlugin;
import es.uvigo.tfg.mantoc.plugins.PluginInfo;
import es.uvigo.tfg.mantoc.plugins.PluginView;

@Extension
public class ContainerPluginContainerControl implements ContainerPlugin{

	@Override
	public PluginInfo getInfo() {
		return new PluginInfo("Container Basic Controll","Plugin for container basic controll","Control Panel");
	}

	@Override
	public boolean isApplicableTo(Container container) {
		
		return true;
	}

	@Override
	public PluginView applyTo(Container container,PluginViewCloseListener listener) {
		
		PluginView panel = new PluginView(this.getInfo().getName()+" of " + container.getName());
		 panel.add(new JLabel("Current Status:"));
		 panel.setOwner(container.getOwnerHost());
		 panel.setPluginCloseListener(listener);
        JTextField status = new JTextField();
        updateStatus(status,container);
        status.setEditable(false);
        panel.add(status, "span");
	        
	    JButton stop = new JButton("Stop container");
	    JButton halt = new JButton("Halt container"); 
	    JButton resume = new JButton("Resume container");
	    
	    panel.add(stop, "span");
	    panel.add(halt, "span");
	    panel.add(resume, "span");
	    stop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				container.getOwnerHost().getHostConnection().getDriver().stopContainer(container);
				 updateStatus(status,container);
			}
	    	
	    });
	    halt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				container.getOwnerHost().getHostConnection().getDriver().haltContainer(container);
				 updateStatus(status,container);
			}
	    	
	    });
	    resume.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				container.getOwnerHost().getHostConnection().getDriver().resumeContainer(container);
				 updateStatus(status,container);
			}
	    	
	    });
	        
	        
		return panel;
	}
	
	private void updateStatus(JTextField text,Container container) {
		ContainerInfo info  = container.getOwnerHost().getHostConnection().getDriver().getContainerInfo(container);
		text.setText(info.getStatus());
		text.repaint();
	}

}
