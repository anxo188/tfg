package es.uvigo.tfg.mantoc.core;

public class DriverInfo {
	private String name;
	private String description;
	private String className;
	public DriverInfo(String name, String description,String className) {
		this.name= name;
		this.description=description;
		this.className = className;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getClassName() {
		return this.className;
	}
	
}
