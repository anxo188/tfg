/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uvigo.tfg.mantoc.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.google.gson.annotations.Expose;

import es.uvigo.tfg.mantoc.plugins.PluginView;

/**
 *
 * @author anxo
 */
public class Host {
	@Expose
	private String name;
	@Expose
	private String description;
	@Expose
	private String direction;
	@Expose
	private String port;
	@Expose
	private String driverName;
	
	private List<Container> containers;
	private HostConnection connection;
	private Map<String, String> additionalParameters;
    @SuppressWarnings("unused")
	private HostInfo info;
    
    public final static Container NULLCONTAINER = new es.uvigo.tfg.mantoc.core.Container(null,"No containers detected", "No containers detected");
    
	public Host() {
		this.containers = new ArrayList<>();
		this.additionalParameters = new HashMap<String, String>();
	}

	public Host(String name, String description) {
		this.name = name;
		this.description = description;
		this.containers = new ArrayList<>();
		this.additionalParameters = new HashMap<String, String>();
	}

	public String getName() {
		if(name!=null)
			return name;
		return new String();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		if(description !=null) 
			return description;
		return new String();
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDirection() {
		if(direction !=null) 
			return direction;
		return new String();
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getPort() {
		if(port !=null && !port.isEmpty()) 
			return port;
		return "000";
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDriverName() {
		return this.driverName;
	}

	public void setDriverName(String newName) {
		this.driverName = newName;
	}

	public Map<String, String> getAdditionalParameters() {
		return additionalParameters;
	}

	public void setAdditionalParameters(Map<String, String> params) {
		this.additionalParameters = params;
	}

	public List<Container> getContainers() {
		return containers;
	}

	public void addContainer(Container container) {
		if (this.containers == null) {
			this.containers = new ArrayList<>();
		}
		container.setOwnerHost(this);
		this.containers.add(container);
	}

	public void removeContainer(Container container) {
		if (this.containers != null) {
			this.containers.remove(container);
			container.setOwnerHost(null);
		}
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + Objects.hashCode(this.name);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Host other = (Host) obj;
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		return true;
	}

	public void setHostConnection(HostConnection connection) {
		this.connection = connection;
	}

	public HostConnection getHostConnection() {
		return this.connection;
	}

	public void setNullContainer() {
		this.containers = new ArrayList<>();
		this.containers.add(NULLCONTAINER);
	}

	public void setContainers(List<Container> containers) {
		this.containers = containers;
	}

}
